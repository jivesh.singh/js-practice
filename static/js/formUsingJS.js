var formElement= document.createElement("form");
document.body.appendChild(formElement);
var tableElement= document.createElement("table");
formElement.appendChild(tableElement);

for(var i=1; i<=3; i++){
    var tableRow= document.createElement("tr");
    var idRow= "tableRow"+i;
    tableRow.setAttribute("id",idRow);
    tableElement.appendChild(tableRow);

    var tableData1= document.createElement("td");
    var idRowData1= "tableData"+i+'-1';
    tableData1.setAttribute("id",idRowData1);
    tableRow.appendChild(tableData1);

    var tableData2= document.createElement("td");
    var idRowData2= "tableData"+i+'-2';
    tableData2.setAttribute("id",idRowData2);
    tableRow.appendChild(tableData2);
}


var labelNum1= document.createElement("label");
labelNum1.setAttribute("for","inputNum1");
labelNum1.innerHTML="Number 1:";
document.getElementById("tableData1-1").appendChild(labelNum1);


var inputNum1= document.createElement("input");
inputNum1.setAttribute("type","text");
inputNum1.setAttribute("id", "inputNum1");
inputNum1.setAttribute("name", "num1");
document.getElementById("tableData1-2").appendChild(inputNum1);


var labelNum2= document.createElement("label");
labelNum2.setAttribute("for","inputNum2");
labelNum2.innerHTML= "Number 2:";
document.getElementById("tableData2-1").appendChild(labelNum2);

var inputNum2= document.createElement("input");
inputNum2.setAttribute("type","text");
inputNum2.setAttribute("id", "inputNum2");
inputNum2.setAttribute("name", "num2");
document.getElementById("tableData2-2").appendChild(inputNum2);

var submitButton= document.createElement("input");
submitButton.setAttribute("type","submit");
submitButton.setAttribute("value","Submit");
document.getElementById("tableData3-2").appendChild(submitButton);