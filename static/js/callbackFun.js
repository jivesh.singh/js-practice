// Implementing callback function


// #1 Basic Implementation of callback function


function show(){
    console.log("I am show function");
}


// Callback is just a parameter name; anything can be passed as parameter name
function fun1(callback){
    callback();
}

fun1(show);


// #2 Callback Function

