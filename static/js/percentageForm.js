// Percentage Calculator

var num = document.getElementById("input-num");

var tot = document.getElementById("input-total");

var res = document.getElementById("result");

var form = document.getElementById("percentCalc");


form.addEventListener('submit', function () {
    var x = Number(num.value);
    var y = Number(tot.value);
    console.log(x);
    console.log(typeof x);

    if (typeof y == 'number' && y != NaN && y >= 0) {

        if (typeof x == 'number' && x != NaN && x >= 0) {
            var percentageCalculate = (x / y) * 100;
            res.innerText = "Result: " + percentageCalculate + " %";
            event.preventDefault();
        }

        else {
            res.innerText = "Error: Invalid value!!! Please Input number only";
            event.preventDefault();
        }

    }

    else {
        res.innerText = "Error: Invalid value!!! Please Input number only";
        event.preventDefault();
    }



});