// Calculate BMI using objects

var john = {
    name: "john",
    mass: 90,
    height: 1.82,

    calcBMI: function () {
        this.BMI = this.mass / (this.height ** 2);
        return this.BMI;
    }
};

var mark = {
    name: "mark",
    mass: 104,
    height: 1.82,

    calcBMI: function () {
        this.BMI = this.mass / (this.height ** 2);
        return this.BMI;
    }
};




if(john.calcBMI()>mark.calcBMI()){
    console.log("John's BMI is greater");
    console.log("Name"+john.name+"\nBMI"+john.BMI);
}
else if(john.calcBMI()<mark.calcBMI()){
    console.log("Mark's BMI is greater");
    console.log("Name"+mark.name+"\nBMI"+mark.BMI);
}
else{
    console.log("John and Mark have equal BMI");
    console.log("Name"+john.name+"\nBMI"+john.BMI);
    console.log("Name"+mark.name+"\nBMI"+mark.BMI);
}