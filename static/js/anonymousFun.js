// Implementing Anonymous Function in JS


// #1 Basic Implementation of anonymous function
var fun1 = function () {
    document.write("1. This is an anonymous function.");
};

fun1();

// #2 Anonymous Function with parameters
var fun2 = function (a, b) {
    document.write("<br><br>2. Anonymous function with Parameters => " + a + ", " + b);
};

fun2(10, "QWERTY");

// #3 Passing Anonymous Function as Arguments
function disp1(fun3) {
    return fun3();     // If () is not given after fun3, then the whole anonymous function declaration will be printed as it is
}

document.write("<br><br>" + disp1(function () {
    return "3. This anonymous function is passed as an argument";
}));

// #4 Returning Anonymous Function

function fun4(a){
    return function(b){
        return a+b;
    };
}

// This way, the anonymous function declaration is printed.
document.write("<br><br><strong>4. Returning Anonymous Function Issue<br><br>"+fun4(10)+"</strong><br><br>");

// Solution to this issue
var abc= fun4(10);
document.write("<br><br><strong> Solution to this Issue:</strong><br><br>"+abc(20));



