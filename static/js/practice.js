// JS Practice

/*
let para = document.createElement("P");
let paraText = document.createTextNode("This is a text");
para.appendChild(paraText);


let element = document.getElementById("div1");
element.appendChild(para);


function fun1() {
    element.innerHTML = "This is another text.";
}
*/

/*
var x = 10;
function test() {
    console.log(x);
    var x = 5;
}

console.log(x);
test();
console.log(x);
*/

/*

let z=10;
function test(){
    console.log(z); //Error
    let z=5;
}

console.log(z);
test();
console.log(z);
*/


// function aa() {
//     var a = "Wuba Luba Dub Dub";
//     return function bb() {
//         var b = "Get Rickity Rickity Wrecked Son";
//         return function cc() {
//             var c = "Cheers To That MF";
//             return function dd() {
//                 console.log(a + b + c);
//             }
//         }
//     }
// }
// v = aa();
// v2= v();
// v3= v2();
// v3();

// To check whether the object contains the given property or not

/*
function propFind(obj,property){
    if(obj.hasOwnProperty(property)){
        console.log(true);
    }

    else{
        console.log(false);
    }
}

let obj1= {
    name: "Jivesh",
    age: 18,
    gender: "M",
};


propFind(obj1,"class");
propFind(obj1,"name");
let p= "class";
propFind(obj1,p);

let q="name";
propFind(obj1,q);
*/




/*
this.car= "Scorpio";

var markGarage={
    car: "Mercedes",
    getcar: function(){
        return this.car;
    }
};

var markCar= markGarage.getcar;

console.log(markCar());     //Output is Scorpio but it should be Mercedes

var realMarkCar= markGarage.getcar.bind(markGarage);

console.log(realMarkCar());
*/




/*
// Output question #1

var x;
console.log(x);     // Output: undefined ; becoz x has been declared but it has not been assigned any values

*/


/*
// Output question #2

console.log(x);     // Output: undefined ; becoz x is accessed before it is declared (hoisting)
var x;
console.log(x);    // Output: undefined ; bcoz x has been declared but it has not been assigned any value
x= 20;
console.log(x);   // Output: 20;
*/



/*
// Output question #3



var y = 1;
if (function f(){}) {
    y += typeof f;
  }
console.log(y); // Output: 1undefined;

// The output below would be 1undefined. The if condition statement evaluates using eval, so eval(function f(){}) returns function f(){} (which is true). Therefore, inside the if statement, executing typeof f returns undefined because the if statement code executes at run time, and the statement inside the if condition is evaluated during run time.


var k = 1;
if (1) {
    eval(function foo(){});
    k += typeof foo;
}
console.log(k); // Output: 1undefined;


var k = 1;
if (1) {
    function foo(){};
    k += typeof foo;
}
console.log(k); //Output: 1function;

*/


/*
// Output question #4

var output = (function(x){
    delete x;
    return x;
})(0);
  
console.log(output); // Output: 0 ;The delete operator is used to delete properties from an object. Here x is not an object but a local variable. delete operators don't affect local variables.
*/


/*

// Output question #5

var x = 1;
var output = (function(){
    delete x;
    return x;
})();
  
console.log(output); // Output: 1;  The delete operator is used to delete the property of an object. Here x is not an object, but rather it's the global variable of type number.

*/



/*
// Output question #6

var x = { foo : 1};
var output = (function(){
    delete x.foo;
    return x.foo;
})();
  
console.log(output);  // Output: undefined 

//The delete operator is used to delete the property of an object. Here, x is an object which has the property foo, and as it is a self-invoking function, we will delete the foo property from object x. After doing so, when we try to reference a deleted property foo, the result is undefined.
*/



/*
// Output question #7

var Employee = {
    company: 'xyz'
}
var emp1 = Object.create(Employee);
delete emp1.company;
console.log(emp1.company);  // Output: xyz 

//The output would be xyz. Here, emp1 object has company as its prototype property. The delete operator doesn't delete prototype property. emp1 object doesn't have company as its own property. You can test it console.log(emp1.hasOwnProperty('company')); //output : false. However, we can delete the company property directly from theEmployee object using delete Employee.company. Or, we can also delete the emp1 object using the __proto__ property delete emp1.__proto__.company.

*/



/*
// Output question #8

var trees = ["redwood","bay","cedar","oak","maple"];
delete trees[3];
console.log(trees);  // Output: (5) ["redwood", "bay", "cedar", empty, "maple"]
*/


/*
// Output question #9

var trees = ["redwood","bay","cedar","oak","maple"];
delete trees[3];
console.log(trees.length); // Output: 5

// The output would be 5. When we use the delete operator to delete an array element, the array length is not affected from this. This holds even if you deleted all elements of an array using the delete operator.
*/



/*

// Output question #10

var bar = true;
console.log(bar + 0);       // Output: 1   
console.log(bar + "xyz");   // Output: truexyz
console.log(bar + true);    // Output: 2
console.log(bar + false);   // Output: 1

*/



/*

// Output question #11

var z = 1, y = z = typeof y;
console.log(y);                 // Output: undefined
console.log(z);                 // Output: undefined

// According to the associativity rule, operators with the same precedence are processed based on the associativity property of the operator. Here, the associativity of the assignment operator is Right to Left, so typeof y will evaluate first , which is undefined. It will be assigned to z, and then y would be assigned the value of z and then z would be assigned the value 1.

*/



/*

// Output question #12

// NFE (Named Function Expression 
var foo = function bar(){ return 12; };
typeof bar();                                  // Output: ReferenceError: bar is not defined

var bar1 = function(){ return 12; };
console.log(typeof bar1());                        // Output: number

function bar2(){ return 12; };
console.log(typeof bar2());                        // Output: number

// A function definition can have only one reference variable as its function name. In sample 1, bar's reference variable points to anonymous function. In sample 2, the function's definition is the name function.

var foo = function bar(){ 
    // foo is visible here 
    // bar is visible here
 	console.log(typeof bar()); // Work here :)
 };
// foo is visible here
// bar is undefined here

*/



/*

// Output question #13

var salary = "1000$";

 (function () {
     console.log("Original salary was " + salary);          // Output: Original salary was undefined

     var salary = "5000$";

     console.log("My New Salary " + salary);                // Output: My New Salary 5000$
})();


//The output would be undefined, 5000$. Newbies often get tricked by JavaScript's hoisting concept. In the code above, you might be expecting salary to retain its value from the outer scope until the point that salary gets re-declared in the inner scope. However, due to hoisting, the salary value was undefined instead.



// Output question #14


// Output same as #13

var salary = "1000$";

(function () {
    var salary = undefined;
    console.log("Original salary was " + salary);

    salary = "5000$";

    console.log("My New Salary " + salary);
})();

*/


/*
// Output question #15

const car = {make: 'Honda', model: 'Accord', year: 1998};
console.log('make' in car);     // true
delete car.make;
if ('make' in car === false) {
car.make = 'Suzuki';
}
console.log(car.make);          // Suzuki
*/


/*
// Random no. from 5-9 (both inclusive)
document.write(Math.floor(Math.random()*5)+5);
*/


/*
// Question #16
// Take a string and add another string infront of the original string and at the back of it.
// Total length of final string equal to given length.
// Eg:- FirstName = 'nitish'
// LastName = 'kr'
// Suppose total length = 10;
// Output will be: krkrnitish and nitishkrkr

// Make a function() takes parameter (total length i.e 10 , and string to be Added i.e LastName).
*/


