/*

// Question #1


//If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

//Finish the solution so that it returns the sum of all the multiples of 3 or 5 below the number passed in.

//Note: If the number is a multiple of both 3 and 5, only count it once.


function solution(number){
    var res=0;
    for(var i=1;i<number;i++){
       if(i%3===0 || i%5===0){
          res+=i;
        }
      
    }
    
    return res;
}

console.log(solution(10));  // Answer should be 23

*/

/*

// Question #2

// In this kata, you must create a digital root function.

// A digital root is the recursive sum of all the digits in a number. Given n, take the sum of the digits of n. If that value has more than one digit, continue reducing in this way until a single-digit number is produced. This is only applicable to the natural numbers.

// Here's how it works:

// digital_root(16)
// => 1 + 6
// => 7

// digital_root(942)
// => 9 + 4 + 2
// => 15 ...
// => 1 + 5
// => 6

// digital_root(132189)
// => 1 + 3 + 2 + 1 + 8 + 9
// => 24 ...
// => 2 + 4
// => 6

// digital_root(493193)
// => 4 + 9 + 3 + 1 + 9 + 3
// => 29 ...
// => 2 + 9
// => 11 ...
// => 1 + 1
// => 2

function digital_root(n) {    
    var digit;
    var sum=0;
    while(n>0){
      digit= n%10;
      sum+=digit;
      n= parseInt(n/10);
    }
    
    if(sum.toString().length!=1){
        return digital_root(sum);
    }

    else{
        return sum; 
    }

    
}

var x= digital_root(16);
console.log(x);
var y= digital_root(426);
console.log(y);
var z= digital_root(1234556);
console.log(z);

*/

/*
// Question #3
// Convert a no. to a string
function numberToString(num){
    //return num.toString();

    var res='';
    res+=num;
    return res;
}

var str= numberToString(23);
console.log(str);
*/

// Question #4
//Create a function unique_in_order which takes as argument a sequence and returns a list of items without any elements with the same value next to each other and preserving the original order of elements.
// For example:

// uniqueInOrder('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
// uniqueInOrder('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
// uniqueInOrder([1,2,2,3,3])       == [1,2,3]



