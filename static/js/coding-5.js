// Tip calculator using Objects and Methods

function avgTip(tip){
    var a=0;
    for(var i=0;i<tip.length;i++){
        a+=tip[i];
    }
    a= a/tip.length;
    return a;
}


var john= {
    bill:[124,48,268,180,42],
    tip:[],
    amount:[],
    calcTip: function(){
        for(var i=0;i<5;i++){
            var m= this.bill[i];
            var t;
            if(m<50){
                t= m*0.2;
                this.tip.push(t);
                this.amount.push(m+t);
            }
            else if(50<=m<200){
                t= m*1.5;
                this.tip.push(t);
                this.amount.push(m+t);
               
            }
            else{
                
                t= m*0.1;
                this.tip.push(t);
                this.amount.push(m+t);
            }
        }
        console.log("John's Tip: "+this.tip);
        console.log("John's Amount: "+this.amount);
    }

};

var mark= {
    bill:[77,375,110,45],
    tip:[],
    amount:[],
    calcTip: function(){
        for(var i=0;i<4;i++){
            var m= this.bill[i];
            var t;
            if(m<100){
                t= m*0.2;
                this.tip.push(t);
                this.amount.push(m+t);
            }
            else if(100<=m<300){
                t= m*0.1;
                this.tip.push(t);
                this.amount.push(m+t);
               
            }
            else{
                
                t= m*0.25;
                this.tip.push(t);
                this.amount.push(m+t);
            }
        }
        console.log("Mark's Tip: "+this.tip);
        console.log("Mark's Amount: "+this.amount);
    }

};

john.calcTip();
mark.calcTip();


var johnAverage= avgTip(john.tip);

var markAverage= avgTip(mark.tip);

console.log("Tip average of John's family: "+johnAverage);
console.log("Tip average of Mark's family: "+markAverage);

if(johnAverage>markAverage){
    console.log("Average tip of John's family is greater");
}
else if(johnAverage<markAverage){
    console.log("Average tip of Mark's Family is greater");
}
else{
    console.log("Average tips of both families are equal");
}